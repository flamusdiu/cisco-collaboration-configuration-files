# Domain Information
$serverName = "WIN-SRV-AD"
$domain = "federation"
$tld = "local"

# IP Information
$networkid = "10.0.0.0"
$ipaddress = "10.0.0.2"
$gateway = "10.0.0.1"
$networksub = "24"

# TimeZone
$timezone = "Eastern Standard Time"

# Network Hosts
$hosts = @(
    ("win-srv-ad", "10.0.0.2"),
    ("pfSense", "10.0.0.1"),
    ("cucm1", "10.0.0.120"),
    ("cup1", "10.0.0.121")
)

# Windows Clients
$winClients = @(
    "WIN-UCS-1",
    "WIN-UCS-2"
)

# Windows Users
$users = @(
    ("Marsha", "Fields", "731331000", "marsha.fields"),
    ("Joe", "Smith", "7313310001", "joe.smith")
)

# Checks for completed tasks
# Be warned this are basic checks and may succeed if the task is partly completely
$computerInfo = Get-ComputerInfo
$timezone_check = $(Get-TimeZone | Select -ExpandProperty "Id") -eq $timezone
$ad_installed = Get-WindowsFeature -name "AD-Domain-Services" | Select -ExpandProperty "Installed"
$dns_installed = Get-WindowsFeature -name "DNS" | Select -ExpandProperty "Installed"
$rename_server = $computerInfo.CsName -eq $serverName
$current_IP = Get-NetIPAddress -AddressFamily IPv4 | Where-Object { $_.IPAddress -ne "127.0.0.1" -and $_.Status -ne "Disconnected" }

# Hides Server Manager Window
New-ItemProperty -Path HKCU:\Software\Microsoft\ServerManager -Name DoNotOpenServerManagerAtLogon -PropertyType DWORD -Value "0x1" –Force | Out-Null

# Configured autologon to run script
function Manage-Autologon ($type) {
    #Registry path declaration
    $RegPath = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
    $RegROPath = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce"

    if ($type -eq "remove") {
        Remove-ItemProperty $RegPath "DefaultUsername"
        Remove-ItemProperty $RegPath "DefaultPassword"
        Remove-ItemProperty $RegROPath "Configure Server"
    } else {

        if ($type -eq "domain") {
            $username = "federation\Administrator"
            $password = "P@55w0rd"
        } else {
            $username = "Administrator"
            $password = "P@55w0rd"
        }

        $autologon = $(Get-ItemProperty $RegPath "AutoAdminLogon" -ErrorAction SilentlyContinue).AutoAdminLogon

        if ( ($autologn -eq 0) -or ($autologon -eq $null) ) {

            #setting registry values
            Set-ItemProperty $RegPath "AutoAdminLogon" -Value "1" -type String       
            Set-ItemProperty $RegPath "AutoLogonCount" -Value "2" -type DWord
        }

        Set-ItemProperty $RegPath "DefaultUsername" -Value "$username" -type String  
        Set-ItemProperty $RegPath "DefaultPassword" -Value "$password" -type String 
        Set-ItemProperty $RegROPath "Configure Server" -Value "Powershell.exe $PSCommandPath" -type String
    }
}

# Renames server and reboots
function Rename-Server() {
    Write-Host "Renaming server (will reboot)"
    Rename-Computer -NewName "$serverName" -Restart
}

# Sets server ip information
function Set-IPAddress() {
    Write-Host "Configuring IP address and DNS Client..."
    Set-NetIPInterface -InterfaceIndex $current_IP.InterfaceIndex -Dhcp Disabled
    New-NetIPAddress -InterfaceIndex $current_IP.InterfaceIndex -PrefixLength 24 -IPAddress $ipaddress -DefaultGateway $gateway | Out-Null
    Set-DnsClientServerAddress -InterfaceIndex $current_IP.InterfaceIndex -ServerAddresses ("127.0.0.1")
    Set-DnsClient -InterfaceIndex $current_IP.InterfaceIndex -ConnectionSpecificSuffix "$domain.$tld"
}

# Sets server timezone
function Set-ServerTime () {
    Set-TimeZone -Id "$timezone"
}

# Installs Active Directory Domain Services
function Create-ADServer() {
    Write-Host "Installing and configuring Active Domain Services..."
    
    Install-WindowsFeature -name AD-Domain-Services -IncludeManagementTools | Out-Null
    Install-ADDSForest -DomainName "$domain.$tld" `
        -DomainNetbiosName $domain `
        -ForestMode WinThresHold `
        -InstallDns `
        -SafeModeAdministratorPassword (ConvertTo-SecureString "P@55w0rd" -AsPlainText -Force) `
        -SkipPreChecks `
        -Force | Out-Null
}

# Configures Active Directory Domain Services 
function Config-ADServer() {
    Write-Host "Creating Active Directory structure..."

    # Active Directory Folders
    New-ADOrganizationalUnit -Name "Corporate" -Path "DC=$domain,DC=$tld"
    foreach ( $o in @('ServiceAccounts', 'Users', 'Computers') ) {
        New-ADOrganizationalUnit -Name "$o" -Path "OU=Corporate,DC=$domain,DC=$tld"
    }

    # Computers
    foreach ($c in $winClients) {
        New-ADComputer -Name $c -SamAccountName $c -Path "OU=Computers,OU=Corporate,DC=$domain,DC=$tld"
    }

    # Users
    foreach ($u in $users) {
        Write-Host "Adding user $($u[0]) $($u[1])"
        New-AdUser -Name "$($u[0]) $($u[1])" `
            -GivenName "$($u[0])" `
            -SurName "$($u[1])" `
            -DisplayName "$($u[1]), $($u[0])" `
            -SamAccountName $u[3] `
            -UserPrincipalName "$($u[3])@$domain.$tld" `
            -Path "OU=Users,OU=Corporate,DC=$domain,DC=$tld" `
            -Enabled $true `
            -ChangePasswordAtLogon $false `
            -AccountPassword (ConvertTo-SecureString "P@55w0rd" -AsPlainText -Force) `
            -PasswordNeverExpires $true `
            -CannotChangePassword $true
    }
    
    Write-Host "Adding user for CUP LDAP Directory Sync"
    $u = "ciscodirsync"
